# RSC-Single-Player
RSC Single Player is a one player RuneScape Classic reproduction. You do not need any sort of internet connection what-so-ever to play. The client handles itself entirely and does not rely on a server.

# Requirements
* Java 8 or newer

# Instructions
* To launch you can either double click the 'rsc-complete.jar' file, or use the run.bat (Windows) or run.sh (Unix)
* To create a new account, click the "New User" button and fill out the information below
* Login and play
    
# Game Information
* Experience modifier is set at 8x
* The game can now be resized to your preferred dimensions
* Social tab and "Report a Player" features have been removed for obvious reasons
* Music player preloaded with tracks (off by default)
* Swap bank items by right clicking on the item
* You can use multiple accounts, but do not use more than one account during the same session (you can have multiple windows open)
* Administrator privileges can be acquired by creating a new account with username "root" and password "devkit", and also supports right-clicking the mini-map to teleport

# Releases
https://github.com/sean-niemann/RSC-Single-Player/releases

# Media

![picture alt](https://nemotech.org/rsc/mopar1.png "RSCSP1")
![picture alt](https://nemotech.org/rsc/mopar2.png "RSCSP2")

# Other Information

JS Version: https://nemotech.org/apps/rsc/ (Use username "root")

Original post: https://forum.moparscape.org/rsc-server-development-discussion/52/release-rsc-single-player-v2-3/672287/
